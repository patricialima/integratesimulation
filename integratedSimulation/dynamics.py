# -*- coding: utf-8 -*-
"""
Created on Thu Apr 02 19:10:05 2015

@author: Ana
"""

import pysces
import random
from collections import *


class dynamicSim(object):

    def __init__(self,sbmlFile,pscfname, density, nrOfiterations, sigma, workDirectory):
        self.sbmlFile = sbmlFile
        self.pscfname = pscfname
        self.density = density
        self.nrOfiterations = nrOfiterations
        self.sigma = sigma
        self.workDirectory = workDirectory
        self.dynamicModel = None        
        self.firstResults = OrderedDict()
        self.dynamicDict = OrderedDict()
        
    def setWorkPath(self):
        pysces.setWorkPath(self.workDirectory)

    def changeDensity(self, dens):
        self.density = dens
    
    def changePSCFname(self, psc):
        self.pscfname = psc
    
    def changeSBMLfile(self, sbml):
        self.sbmlFile = sbml
    
    def convertSBMLtoPSC(self):
        pysces.interface.convertSBML2PSC(self.sbmlFile, pscfile = self.pscfname, sbmldir =self.workDirectory,pscdir= self.workDirectory)
        self.dynamicModel = pysces.model(self.pscfname, dir = self.workDirectory)

    def convert(self, typeofconversion, value):
        if typeofconversion == 1:
            newValue = (value*3600)/float(self.density) 
        elif typeofconversion == 2:
            newValue = (value*self.density)/3600.0
        else:
            return "error"
        
        return newValue       
    
    
    def variateParemeters(self):
        factor = random.lognormvariate(0,self.sigma)  
        for i in self.dynamicModel.parameters:
            if ('_rmax' in i) or ('_K' and 'eq' in i) or ('_k' and 'eq' in i):
                b = getattr(self.dynamicModel, i)
                setattr(self.dynamicModel,i,(b*factor))
      
    
    def createDictResults(self, typeOfconvert,dilution):
        self.dynamicModel.Dil = dilution
        
        for r in self.dynamicModel.reactions:
            self.firstResults[r] = []
        
        for i in xrange(self.nrOfiterations):
            self.variateParemeters()
            self.dynamicModel.doState()
            for r_id in self.dynamicModel.reactions:
                val = getattr(self.dynamicModel.data_sstate,r_id)
                flux = self.convert(typeOfconvert, val)
                self.firstResults[r_id].append(flux)
            
            print 'iteration %d of %d for dilution %f is complete' %(i, (self.nrOfiterations-1), dilution)
        
        return self.firstResults

    def dynamicSimulation(self, conversionType, dilution = []):
        self.convertSBMLtoPSC()
        
        if len(dilution) == 0: dilution.append(self.dynamicModel.Dil)
            
        results = [self.createDictResults(conversionType, dil) for dil in dilution]
        
        for key in results[0].keys():
            self.dynamicDict[key] = [] 
    
        for k in results[0].keys():
            for b in range(len(results)):
                self.dynamicDict[k].append(results[b][k])
        
        print 'Dynamic Simulation: Done'


    def getBounds(self, listOfval):    
        mx = max(listOfval)    
        mn = min(listOfval)
        return (mn,mx)
        
    
    def mapDynValues(self):
        mapDict = OrderedDict()
        
        for reactionID in self.dynamicDict:
            mapDict[reactionID] = []
    
        for i in self.dynamicDict:
            for j in range(len(self.dynamicDict[i])):
                mapDict[i].append(self.getBounds(self.dynamicDict[i][j]))
        
        return mapDict
    


