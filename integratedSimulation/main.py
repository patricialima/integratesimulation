# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 00:24:05 2015

@author: Ana
"""
from dynamics import dynamicSim
from constraintBased import constraintBasedSim
from utility import createCondFile, integrate

  
if __name__ == '__main__':
    DYNfile = 'DynamicModel.xml'
    CBfile = 'Ec_iAF1260.xml'
    directory = 'C:\Users\Ana\Documents\GitHub\integrateSimulation\integratedSimulation'
    sigma = 2
    dynamicModel = dynamicSim(DYNfile,'dynamics', 564, 1000, sigma ,directory)
    dynamicModel.setWorkPath()
    dynamicModel.dynamicSimulation(1)
    mapBounds = dynamicModel.mapDynValues()
    a = integrate('mapping.csv', mapBounds)
    conditionFile = [createCondFile('caseOftest.txt', a)]
    eps = 0.0001
    solver = 'gurobi'
    cbmodel = constraintBasedSim(CBfile,eps,solver,conditionFile)
    cbmodel.getCobraModel()
    cbmodel.getFinalresults()
    print 'Simulation is complete with a solution: %f' %(cbmodel.solution)