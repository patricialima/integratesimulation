# -*- coding: utf-8 -*-
"""
Created on Thu Apr 02 19:09:53 2015

@author: Ana
"""

import cobra
from cobra.io import read_sbml_model
from cobra.flux_analysis.parsimonious import optimize_minimal_flux
from collections import OrderedDict
from utility import readConditions

class constraintBasedSim(object):
   

    def __init__(self,sbmlFile, epsilon, solver, conditionFiles):
        self.sbmlfile = sbmlFile
        self.epsilon = epsilon
        self.solver = solver
        self.condfiles = conditionFiles
        self.cbmodel = None
        self.solution = None
        self.firstResults = OrderedDict()
        self.cbDict = OrderedDict()
        
    
    def getCobraModel(self):
        self.cbmodel = read_sbml_model(self.sbmlfile, old_sbml=True)

    def simulation(self, conditions):
        [setattr(x,'objective_coefficient', 0.) for x in self.cbmodel.reactions]
        self.cbmodel.reactions.ATPM.lower_bound = 0        
        objective_reaction = self.cbmodel.reactions.get_by_id(conditions[0][1])
        objective_reaction.objective_coefficient = int(conditions[0][2])  
        flux_restric = [c for c in conditions if  'objective function' not in c[0]]
        for i in range(len(flux_restric)):
            react = self.cbmodel.reactions.get_by_id(flux_restric[i][0])
            if react.lower_bound >= 0:
                react.lower_bound = max(float(flux_restric[i][1])-self.epsilon, 0)
            else:
                react.lower_bound = float(flux_restric[i][1])-self.epsilon
            
            react.upper_bound = float(flux_restric[i][2])+self.epsilon
        try:
            pFBA = optimize_minimal_flux(self.cbmodel, solver = self.solver)
            self.firstResults = pFBA.x_dict
            self.solution = pFBA.f
            print 'Solution found.'
        except:
            print 'Error: infeasible.'  
        
        
    
    def getFinalresults(self):
        results = []
        for condFile in self.condfiles:
            self.simulation(readConditions(condFile))
            results.append(self.firstResults)
        for i in xrange(len(results[0].keys())):
            self.cbDict[self.cbmodel.reactions[i].id] = []
            for j in xrange(len(self.condfiles)):
                self.cbDict[self.cbmodel.reactions[i].id].append(results[j][self.cbmodel.reactions[i].id])


