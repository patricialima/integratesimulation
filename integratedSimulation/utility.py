# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 11:52:04 2015

@author: Ana
"""
from collections import OrderedDict
import pandas as pd


    
def readConditions(conditionFile ):
    with open(conditionFile, 'r') as condFile:
        conditions = [line.replace('\n','').split(',') for line in condFile.readlines()]
    return conditions


def integrate(mappingfile, bounds):
    dynamic = []
    stoi =[]
    integration = OrderedDict()
    mapfile = open(mappingfile,'r')    
    for line in mapfile.readlines():
        if 'Reaction' in line:
            continue
        d = line.replace('\n','').split(',')
        dynamic.append(d[2])
        stoi.append(d[1])
    for key in bounds.keys():
        if key in dynamic:
            r_id = stoi[dynamic.index(key)]
            integration[r_id] = [bounds[key][0][0],bounds[key][0][1]]
    return integration

def createCondFile(name, mapDict,objectiveFunction = 'Ec_biomass_iAF1260_core_59p81M'):
    cond = open(name,'w')
    cond.writelines(['objective function,',objectiveFunction,',1','\n'])
    for key in mapDict.keys():
        cond.writelines([str(key),',',str(mapDict[key][0]),',',str(mapDict[key][1]),'\n'])
    cond.close()
    return name
    
def mapWithExpdata(experimentalFile):
    df = pd.read_csv(experimentalFile)
    n = 0
    justalist =[]
    kegg_ids = OrderedDict()
    stoidict = OrderedDict()
    dyndict = OrderedDict()
    for i in df.columns:
        if 'Case' in i:
            n+=1
    for line in range(len(df)):
        b = df.irow(line)[-n:]
        justalist.append(list(b))
        kegg_ids[df.irow(line)[0]] = justalist[line]
        stoidict[df.irow(line)[0]]=df.irow(line)[1]
        dyndict[df.irow(line)[0]]=df.irow(line)[2]
    return kegg_ids, stoidict, dyndict

def checkReactDirection(fluxList):
    direction = []
    for i in fluxList:
        if i >= 0:
            direction.append(1)
        elif i<0:
            direction.append(-1)
    return direction
        