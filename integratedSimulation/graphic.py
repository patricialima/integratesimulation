# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 00:05:29 2015

@author: Ana
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


class graphic(object):


    def __init__(self, x, y, dimension):
        self.x = x
        self.y = y
        self.dimension = dimension
        
        
    def createGraphic(self):
        f,ax = plt.subplots(self.dimension[0],self.dimension[1],sharex=True, sharey=True,figsize=(self.dimension[1]*4,self.dimension[0]*4))
        return f,ax

    def plotValues(self, title, xlabel, ylabel):
        x,y = np.array(self.x), np.array(self.y)    
        trendLine = numpy.polyfit(x, y, 1)
        p = np.poly1d(trendLine)
        min_xy = min(min(x), min(y))
        max_xy = max(max(x), max(y))
        self.createGraphic[1].plot([min_xy, max_xy],[min_xy, max_xy],'k--')
        self.createGraphic[1].plot(x,y,'o')
        self.createGraphic[1].plot(x,p(x), 'k-')
        self.createGraphic[1].set_xlabel(xlabel)
        self.createGraphic[1].set_ylabel(ylabel)
        self.createGraphic[1].set_xlim((min_xy-1, max_xy+1))
        self.createGraphic[1].set_ylim((min_xy-1, max_xy+1))
        self.createGraphic[1].legend(['Fluxes Data', "y=%.2fx+(%.2f)"%(p[1],p[0])],"upper left",prop={'size':7})
        self.createGraphic[1].set_title(title)
    
    def mainGraphic(figure, listOfax, listOfFluxes,names = ['Exp','Stoi','Dyn']):    
        for i in range(len(listOfFluxes[0])):
            idx = 0
            for j in range(len(listOfFluxes)):
                for k in range(j+1,len(listOfFluxes)):
                    plotValues(x = listOfFluxes[j][i],y = listOfFluxes[k][i],ax=listOfax[i][idx], title = '%s vs %s' %(names[j],names[k]), xlabel=names[j], ylabel=names[k])
                    idx +=1               

        